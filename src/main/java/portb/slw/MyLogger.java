package portb.slw;

@SuppressWarnings("unused")
public interface MyLogger
{
    void info(String msg);
    
    void debug(String msg);
    
    void error(String msg);
    
    void error(String msg, Throwable e);
    
    void warn(String msg);
}
