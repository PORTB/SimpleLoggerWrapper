package portb.slw;

public class MyLoggerFactory
{
    private final static MyLogger EMPTY_LOGGER = new MyLogger() {
        //region stub
        @Override
        public void info(String msg)
        {
        
        }
    
        @Override
        public void debug(String msg)
        {
        
        }
    
        @Override
        public void error(String msg)
        {
        
        }
    
        @Override
        public void error(String msg, Throwable e)
        {
        
        }
    
        @Override
        public void warn(String msg)
        {
        
        }
        //endregion
    };
    
    public static MyLogger createMyLogger(org.slf4j.Logger logger)
    {
        return new MyLogger() {
            @Override
            public void info(String msg)
            {
                logger.info(msg);
            }
    
            @Override
            public void debug(String msg)
            {
                logger.debug(msg);
            }
    
            @Override
            public void error(String msg)
            {
                logger.error(msg);
            }
    
            @Override
            public void error(String msg, Throwable e)
            {
                logger.error(msg, e);
            }
    
            @Override
            public void warn(String msg)
            {
                logger.warn(msg);
            }
        };
    }
    
    public static MyLogger createMyLogger(org.apache.logging.log4j.Logger logger)
    {
        return new MyLogger() {
            @Override
            public void info(String msg)
            {
                logger.info(msg);
            }
            
            @Override
            public void debug(String msg)
            {
                logger.debug(msg);
            }
            
            @Override
            public void error(String msg)
            {
                logger.error(msg);
            }
            
            @Override
            public void error(String msg, Throwable e)
            {
                logger.error(msg, e);
            }
            
            @Override
            public void warn(String msg)
            {
                logger.warn(msg);
            }
        };
    }
    
    public static MyLogger emptyLogger()
    {
        return EMPTY_LOGGER;
    }
    
}
